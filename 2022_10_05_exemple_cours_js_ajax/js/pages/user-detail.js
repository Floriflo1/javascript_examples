import { getParam } from "../utils/utils.js";

const $userDetails = document.querySelector("#user");
const $imageCard = document.querySelector("#imageCard");

let paramId = getParam("id");

if (paramId !== null) {
  axios
    .get(`https://jsonplaceholder.typicode.com/users/${paramId}`)
    .then((response) => {
      //   console.log(response.data);
      const user = response.data;

      $imageCard.src = `https://robohash.org/${paramId}`;
      const $nameP = document.createElement("p");
      $nameP.innerText = user.name;
      const $mailP = document.createElement("p");
      $mailP.innerText = user.email;
      const $phoneP = document.createElement("p");
      $phoneP.innerText = user.phone;

      $userDetails.appendChild($nameP);
      $userDetails.appendChild($mailP);
      $userDetails.appendChild($phoneP);
    });
}
