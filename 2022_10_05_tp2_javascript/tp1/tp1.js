const $changeColorButton = document.querySelector("#changeColor");
const $changeColorHexButton = document.querySelector("#changeColorHex");

function changeColor() {
  const colors = ["green", "red", "rgba(133,122,200)", "#f15025"];

  const $body = document.querySelector("body");

  let newColorIndex = getRandomInt(colors.length);

  $body.style.backgroundColor = colors[newColorIndex];
}

function changeColorHex() {
  //const colors = ["green", "red", "rgba(133,122,200)", "#f15025"];

  const colors = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F"];

  const $body = document.querySelector("body");

  let hexColorString = "#";

  for (let i = 0; i < 3; i++) {
    let newColorIndexNumber = getRandomIntInclusive(0, 9);
    hexColorString += colors[newColorIndexNumber];
  }

  for (let i = 0; i < 3; i++) {
    let newColorIndexLetter = getRandomIntInclusive(10, colors.length - 1);
    hexColorString += colors[newColorIndexLetter];
  }

  //let newColorIndexNumber = getRandomIntInclusive(0, 10);
  //let newColorIndexLetter = getRandomIntInclusive(11, colors.length);

  //$body.style.backgroundColor = colors[newColorIndex];

  $body.style.backgroundColor = hexColorString.toLowerCase();
}

$changeColorButton.addEventListener("click", function () {
  changeColor();
});

$changeColorHexButton.addEventListener("click", function () {
  changeColorHex();
});

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min); // The maximum is inclusive and the minimum is inclusive
}
