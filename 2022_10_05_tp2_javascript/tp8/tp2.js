//https://jsonplaceholder.typicode.com/users
//https://robohash.org
const $search = document.querySelector("#search");
const $searchError = document.querySelector("#search_error");
const $searchForm = document.querySelector("form");
let $results = document.querySelector("#results_list");

$searchForm.addEventListener("click", function (e) {
  e.preventDefault();
  if ($search.value.length === 0) {
    $searchError.innerText = "La recherche ne peut pas être vide";
    return;
  } else {
    $searchError.innerText = "";
  }

  axios
    .get(
      `https://en.wikipedia.org/w/api.php?action=query&list=search&srlimit=20&format=json&origin=*&srsearch=${$search.value}`
    )
    .then((response) => {
      //   console.log(response.data);
      const searchResults = response.data.query.search;

      for (let result of searchResults) {
        let $p = document.createElement("p");
        let $link = document.createElement("a");
        $link.href = `http://en.wikipedia.org/?curid=${result.pageid}`;
        $link.title = result.snippet;
        $link.innerText = result.title;

        $p.append($link);
        $results.appendChild($p);
      }
    });
});
