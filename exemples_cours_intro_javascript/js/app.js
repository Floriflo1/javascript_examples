/*let nombre1 = 45;
let nombre2 = 55;
let somme = nombre1 + nombre2;
// console.log(somme);
console.log(nombre1 + " + " + nombre2 + " = " + somme);

// Template strings
console.log(`${nombre1} + ${nombre2} = ${somme}`);

// Affichage sans template strings
console.log(
  "Je suis\nTu es\nElle/Il est\nNous sommes\nVous êtes\nElles/Ils sont"
);

// Affichage avec template strings
console.log(`
Je suis
Tu es
Elle/Il est
Nous sommes
Vous êtes
Elles/Ils sont
`);

// lecture clavier
let prenom = prompt("Votre prenom: ");
let nom = prompt("Votre nom: ");
let age = prompt("Votre age: ");
alert(`Vous vous appelez ${prenom} ${nom}, et vous avez ${age} ans.`);

let age1 = 17;

if (age1 >= 18) {
  console.log("Grand garçon!!!");
} else {
  console.log("Petit garçon!!!");
}

// Ici, on ne compare que les valeurs, et non les types
// Pas recommandée
if (age1 == "17") {
  console.log("Ouii");
}

// Ici,  il s'agit d'une égalité stricte.Parce qu'on compare les types
// et les valeurs
if (age1 === "17") {
  console.log("Noonnn");
}
*/
/**
 * Opérateurs de comparaison & logiques
 *   == : égalité
 *   === : égalité stricte
 *   >= : Supérieur ou égal
 *  <= : Inférieur ou égal
 *   != : Différent
 *   !== : Différence strict
 *   && : ET/AND
 *   || : OU/OR
 *   !
 */
/*
// Boucle while
const codePin = parseInt(prompt("Définir un code PIN: "));

let codePinATester;
while (codePinATester !== codePin) {
  codePinATester = parseInt(
    prompt("Veuillez un code pour débloquer l'appareil : ")
  );
  if (codePinATester !== codePin) {
    alert("Code PIN erroné, veuillwez recommencer.");
  }
}

// Boucle for
for (let i = 0; i <= 10; i++) {
  console.log(i);
}*/

// NOUVELLE VERSION

// Boucle while : v1
// const codePin = parseInt(prompt(“Définir un code PIN: “));

// let codePinATester;
// codePIN : 1234
// codePinATester : 442
// while (codePinATester !== codePin) {
//   codePinATester = parseInt(
//     prompt(“Veuillez entrer un code pour débloquer l’appareil : “)
//   );
//   if (codePinATester !== codePin) {
//     alert(“Code PIN erroné, veuillez recommencer.“);
//   } else {
//     alert(“Bienvenue!!!!!!!“);
//   }
// }

//1. Limiter les essais
// 2. Entrer que des chiffres
// 3. Duplication du test(Faire un premier test en dehors de la boucle)
// Boucle while : v1
// const codePin = parseInt(prompt(“Définir un code PIN: “));

// let codePinATester;
// while (true) {
//   codePinATester = parseInt(
//     prompt(“Veuillez entrer un code pour débloquer l’appareil : “)
//   );
//   if (codePinATester !== codePin) {
//     alert(“Code PIN erroné, veuillez recommencer.“);
//   } else {
//     alert(“Bienvenue!!!!!!!“);
//     break;
//   }
// }
/*
// v3
const codePin = parseInt(prompt(“Définir un code PIN: “));

let codePinATester;
while (true) {
  codePinATester = parseInt(
    prompt(“Veuillez entrer un code pour débloquer l’appareil : “)
  );
  if (codePinATester === codePin) {
    alert(“Bienvenue!!!!!!!“);
    break;
  }
  alert(“Code PIN erroné, veuillez recommencer.“);
}*/
