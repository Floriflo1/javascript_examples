/**
 * TP1
 * On vous donne un tableau d'entiers et un entier K comme arguments.
 * Retournez un nouveau tableau contenant uniquement les entiers qui sont à K éléments de la fin du tableau.
 * Supposez que le tableau d'arguments aura toujours une taille d'au moins K entiers.
 */
function tp1(tab, limitNumber) {
  /*let returnTab = [];
  for (let i = tab.length - 1; i >= 0; i--) {
    if (i >= tab.length - limitNumber) {
      returnTab.push(tab[i]);
    }
  }
  return returnTab;*/
  let returnTab = [];
  for (let i = tab.length - limitNumber; i < tab.length; i++) {
    returnTab.push(tab[i]);
  }
  return returnTab;

  /*
  Other method :
  return k <= t.length ? t.slice(t.length - k, t.length) : "invalide";
  // The slice() method returns selected elements in an array, as a new array
  */
  /*
  Other method prof
  const solve = (intArray, k) => {
    const arr=[];
    for(let i=0;i<k;i++){
      arr.push(intArray[intArray.length-1-i])
    }
    return arr.reverse();
  };
  */
}

let ex1tp1 = tp1([1, 2, 3, 4], 2);
console.log("TP1 Ex1 : ");
console.log(ex1tp1);

let ex2tp1 = tp1([10, 20, 30, 40, 50, 60], 4);
console.log("TP1 Ex2 : ");
console.log(ex2tp1);

let ex3tp1 = tp1([1, -2, 3, -4], 1);
console.log("TP1 Ex3 : ");
console.log(ex3tp1);

/**
 * TP2
 * On vous donne en argument un tableau d'entiers non négatifs.
 * Retournez le premier élément trouvé dans le tableau dont l'indice est égal à sa valeur.
 */
function tp2(tab) {
  for (let i = 0; i < tab.length; i++) {
    if (i === tab[i]) {
      return i;
    }
  }
}

let ex1tp2 = tp2([10, 20, 11, 12, 4]);
console.log("TP2 Ex1 : ");
console.log(ex1tp2);

let ex2tp2 = tp2([2, 1, 11, 12, 4]);
console.log("TP2 Ex2 : ");
console.log(ex2tp2);

/**
 * TP3
 * On vous donne une chaîne de caractères et un nombre entier K comme arguments.
 * Divisez la chaîne de caractères en un tableau de chaînes de caractères, où
 * chaque élément n'a pas plus de K caractères.
 * Retournez le tableau résultant.
 * La chaîne en argument sera toujours d'au moins K caractères.
 */
function tp3(tab, nbCaracToDivide) {
  /*
  let tabSplit = [];
  let str = "";

  for (let i = 0; i < tab.length; i++) {
    if (i % nbCaracToDivide === 0 && i !== 0) {
      tabSplit.push(str);
      str = "";
    } else {
      str += tab[i];
    }
  }
  return tabSplit;
  */
  let tabSplit = [];
  let str = "";
  let indexLastSplit = 0;
  for (let i = 0; i < tab.length; i++) {
    if (i % nbCaracToDivide === 0 && i !== 0) {
      tabSplit.push(str);
      str = "";
      indexLastSplit = i;
    } else {
      str += tab[i];
    }
  }

  if (indexLastSplit !== tab.length) {
    let caracleft = "";
    for (let i = indexLastSplit; i < tab.length; i++) {
      caracleft += tab[i];
    }
    tabSplit.push(caracleft);
  }
  return tabSplit;
}

let ex1tp3 = tp3("aabbbcccd", 3);
console.log("TP3 Ex1 : ");
console.log(ex1tp3);

let ex2tp3 = tp3("abcdefgh", 5);
console.log("TP3 Ex2 : ");
console.log(ex2tp3);

/**
 * TP5
 *
 */
function tp5() {
  /* Other method
  const allEvenInTabAndMultipleOfNombre = (TableauEntiers, nombre) =>{
    let estMultipleDeKEtDeux = true;
    TableauEntiers.forEach((number) => {
      if (number % nombre !== 0 || nombre % 2 !== 0) {
        estMultipleDeKEtDeux = false;
      }
    });
    return estMultipleDeKEtDeux;
  }
   */
  /* Other method prof
  function solve(intArray,k){
      return intArray.every((number)=>number%2===0 && number%k===0)
    }
  */
}

/**
 * TP7
 *
 */
function tp7(table7, k) {
  /* Other method
  let a = 1;
  const newTable = [];
  while (a <= k) {
    newTable[0] = table7[table7.length - 1];
    for (let i = 1; i < table7.length; i++) {
      newTable[i] = table7[i - 1];
    }
    for (let j = 0; j < table7.length; j++) {
      table7[j] = newTable[j];
    }
    a++;
  }
  return newTable;*/
  /* Other Method
  function rotationTab(tab){
    let lastElement = tab[tab.length-1];
    for(let i = tab.length-1; i>0 ;i--){
        tab[i] = tab[i-1];
    }
    tab[0] = lastElement;
    return tab;
  }
  function rotationTabKTimes(tab, intK){
      for(let i=0;i<intK;i++){
          rotationTab(tab);
      }
      return tab;
  }
  */
  /* Other method
  function tpFunction(tableEntier, entier) {
    const size = tableEntier.length - entier;
    const tableEntierSlice = tableEntier.slice(0, size);
    const tableEntierSlice2 = tableEntier.slice(size);
    newValue = tableEntierSlice2.concat(tableEntierSlice);
    return newValue;
  }
  */
  /* Other methode prof 
  Rotate right
  function rotateRight(array,numbreOfRotations){
    const rotatedArray=[...array]
    for(let i=1;i<=numbreOfRotations;i++){
        const poped=rotatedArray.pop();
        rotatedArray.unshift(poped)
    }
    return rotatedArray;
  }
  Rotate left
  function rotateLeft(array,numbreOfRotations){
    const rotatedArray=[...array]
    for(let i=1;i<=numbreOfRotations;i++){
        const shifted=rotatedArray.shift();
        rotatedArray.push(poped)
    }
    return rotatedArray;
  }
 */
}
