//Les champs du formulaire
const $name = document.querySelector("#name");
const $email = document.querySelector("#email");
const $password = document.querySelector("#password");

// Les paragraphes pour afficher les erreurs
const $nameError = document.querySelector("#name_error");
const $emailError = document.querySelector("#email_error");
const $passwordError = document.querySelector("#password_error");

// Le paragraphe pour afficher le succes pour l'ajout
const $addSuccess = document.querySelector("#add_success");

//tbody
const $users = document.querySelector("#users");

// Le formulaire
const $registerForm = document.querySelector("form");

const $notifications = document.querySelector(".notification");
function addNotification() {
  let not = document.createElement("p");
  let btn = document.createElement("button");
  btn.innerText = "exit";
  not.innerText = "Added with success!";
  not.appendChild(btn);
  $notifications.appendChild(not);
  let timing = setTimeout(function () {
    not.remove();
  }, 5000);
  btn.addEventListener("click", function (e) {
    clearTimeout(timing);
    e.stopPropagation();
    not.remove();
  });
}

$registerForm.addEventListener("submit", function (e) {
  e.preventDefault();
  // console.log(`Nom : ${$name.value}`);
  // console.log(`Email : ${$email.value}`);
  // console.log(`Password : ${$password.value}`);
  let errorFound = false;

  // 1. Ne pas ajouter d'enregistrements dans le tableaux tant que tous les champs ne sont pas remplis
  if ($name.value.length === 0) {
    $nameError.innerText = "Le nom ne peut pas être vide";
    errorFound = true;
  } else {
    $nameError.innerText = "";
  }

  if ($email.value.length === 0) {
    $emailError.innerText = "L'email ne peut pas être vide";
    errorFound = true;
  } else {
    // 2. Terminer la validation du formulaire(email, mot de passe)
    let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!$email.value.match(mailformat)) {
      $emailError.innerText = "L'email n'est pas valide !";
      errorFound = true;
    } else {
      $emailError.innerText = "";
    }
  }

  if ($password.value.length === 0) {
    $passwordError.innerText = "Le mot de passe ne peut pas être vide";
    errorFound = true;
  } else {
    // 2. Terminer la validation du formulaire(email, mot de passe)
    if ($password.value.length < 8) {
      $passwordError.innerText =
        "Le mot de passe doit avoir minimum 8 caractères";
      errorFound = true;
    } else {
      $passwordError.innerText = "";
    }
  }

  if (errorFound) {
    return;
  }

  const $userRow = document.createElement("tr");
  const $nameColumn = document.createElement("td");
  $nameColumn.innerText = $name.value;

  const $emailColumn = document.createElement("td");
  $emailColumn.innerText = $email.value;

  // 4. Ajouter un bouton pour chaque ligne qui permet supprimer la ligne
  const $deleteButton = document.createElement("button");
  $deleteButton.innerText = "Delete button";

  $deleteButton.addEventListener("click", function () {
    $users.removeChild($userRow);
    e.stopPropagation();
  });

  const $deleteColumn = document.createElement("td");
  $deleteColumn.append($deleteButton);

  $userRow.append($nameColumn, $emailColumn, $deleteColumn);
  $users.appendChild($userRow);

  // 5. Vider le formulaire après ajout des données dans le tableau.
  $registerForm.reset();

  // 6. Si les données sont ajoutées avec succès, ajout un message en vert au dessus
  // du formulaire disant "Utilisateur ajouté avec succès et qui va disparaitre après 5 secondes".
  // Sinon, donner à l'utilisateur la possibilité de la fermer.
  addNotification();

  // 1. Ne pas ajouter d'enregistrements dans le tableaux tant que tous les champs ne sont pas remplis
  // 2. Terminer la validation du formulaire(email, mot de passe)
  // 3. Le mot de passe doit avoir au moins 8 caractères
  // 4. Ajouter un bouton pour chaque ligne qui permet supprimer la ligne
  // 5. Vider le formulaire après ajout des données dans le tableau.
  // 6. Si les données sont ajoutées avec succès, ajout un message en vert au dessus
  // du formulaire disant "Utilisateur ajouté avec succès et qui va disparaitre après 5 secondes".
  // Sinon, donner à l'utilisateur la possibilité de la fermer.
});
