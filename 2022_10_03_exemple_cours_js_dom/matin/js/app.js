const framworks = [
  "Nest.js",
  "Spring",
  "Asp Dotnet Core",
  "Ruby On Rails",
  "Angular",
  "Symfony",
  "Laravel",
  "Django",
  "Express.js",
  "Meteor",
];

const technos = document.querySelectorAll("li");

// Afficher toute la liste
// console.log(technos);

// Accéder au premier element de la liste
// console.log(technos[0]);

// O(n)
// for (let element of technos) {
//   const randomIndex = Math.floor(Math.random() * framworks.length);
//   const framwork = framworks[randomIndex];
//   framworks.splice(randomIndex, 1);
//   element.innerText = framwork;
// }

//tb = 1 2 3 4 5 6 7 8 9 10

// 0 1 2 3 4 5 6 7 8 9

//tb.splice(4,1) => 5 est supprimé O(n)

// tb.push() : constante => O(1)
// tb.pop()  : constante => O(1)
// tb.shift() : linéaire => O(n)
// tb.unshift(valeur) => O(n)

// Exo :
function addTextBeforeElem(clasName, textToAdd) {
  const frontClassElements = document.querySelectorAll(clasName);

  for (let elem of frontClassElements) {
    elem.innerText = `${textToAdd} : ${elem.innerText}`;
  }
}

/* Other method JCB
 function addPrefix(strClass) {
  //Finding the elements belonging to a given class
  const queryTab = document.querySelectorAll("." + strClass);
  for (let element of queryTab) {
    //Capitalization
    // let prefix = strClass[0].toUpperCase() + strClass.slice(1);
    //Adding the prefix
    element.innerText = `${strClass} :  ${element.innerText}`;
  }
}

addPrefix("frontend");
addPrefix("server-side");
addPrefix("database");
 */

/*

 */

// Pour la classe frontend, afficher les données avec le prefexe front:
// Front : HTML
// Front : CSS
// Front : Javascrip
//...

// On utilise pas document.getElementsByClassName("...")) car on peut pas utiliser le forEach avec cette methode
// alors qu'avec document.querySelectorAll(".frontend") on peut utiliser forEach

addTextBeforeElem(".frontend", "Front");

// Pour la classe server-side, afficher les données avec le prefexe front:
// Backend : Nest
//..

addTextBeforeElem(".server-side", "Server side");

// Pour la classe database, afficher les données avec le prefexe front:
// Database : PostreSQL

addTextBeforeElem(".database", "Database");
