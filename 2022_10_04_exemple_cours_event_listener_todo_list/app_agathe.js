const todos = [
  "Text a friend I haven't talked to in a long time",
  "Plan a vacation I've always wanted to take",
  "Take cat on a walk",
  "Go to the gym",
  "Have a photo session with some friends",
  "Learn calligraphy",
  "Learn Express.js",
  "Plan a trip to another country",
  "Take a hike at a local park",
  "Bake pastries for me and neighbor",
  "Contribute code or a monetary donation to an open-source software project",
  "Watch a classic movie",
  "Do something nice for someone I care about",
];

const $addTodoButton = document.querySelector("#add");
const $todoList = document.querySelector("#todos");
const $clearTodoButton = document.querySelector("#clear");

let currentIndex = 0;

$clearTodoButton.disabled = true;
$addTodoButton.addEventListener("click", function () {
  //   Arrêter l'ajout des todos quand on arrive à la fin de la liste
  if (currentIndex === todos.length) {
    $addTodoButton.disabled = true;
    $clearTodoButton.disabled = false;
    return;
  } else {
    //   console.log(todos[currentIndex]);
    const $todo = document.createElement("li");
    // rayé le texte lorsqu'on clique
    $todo.addEventListener("click", function () {
      $todo.classList.toggle("completed");
    });

    // bouton supprimer ligne

    // $buttontoRemove.innerHTML =
    // <button class="remove">Remove</button>;

    const $buttontoRemove = document.createElement("button");
    $buttontoRemove.innerText = "X";
    //$todo.innerText = todos[currentIndex]; // ====> Cette ligne etait la ligne 48 elle passe maintenant a la ligne 45
    // $todoList.appendChild($todo);
    $todo.append($buttontoRemove);

    $todo.append(todos[currentIndex]);
    $todoList.append($todo);
    currentIndex++;
  }
  //   1: Vider la liste(ul) quand on clique sur le bouton "clean"
  $clearTodoButton.addEventListener("click", function () {
    $todoList.replaceChildren();
    $addTodoButton.disabled = false;
    $clearTodoButton.disabled = true;
  });

  //   2: Ajouter un bouton supprimer pour chaque todo. Quand on clique sur le
  //      bouton supprimer, retirer le todo de la liste(ul). quelques recherches sur internet
  //   3: Au départ, le bouton clear doit être inactif.Il ne devient actif que lorsqu'on
  //     affiche tous les elements du tableau "todos"
});
