// Création d'un tableau vide
/**
 * 1. Écrivez un programme javascript qui calcule la somme de tous les éléments d’un tableau.
 * 2. A partir de l’exercice précédent, ajouter le calcul de la moyenne des nombres contenus dans le tableau.
 */

let myTab = [];
let sum = 0;

myTab.push(1);
myTab.push(2);
myTab.push(3);

// Sum
for (let i = 0; i < myTab.length; i++) {
  sum += myTab[i];
}

console.log(`tab: ${myTab}`);
console.log(`Sum of tab : ${sum}`);
console.log(`Mean / average : ${sum / myTab.length}`);
