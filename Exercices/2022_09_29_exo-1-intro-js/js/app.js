/**
 * 1. Ecrire un programme Javascript qui demande à l’utilisateur d’entrer à partir du clavier:
 * La distance parcouru (m)
 * Le temps(sec)
 * Puis calculer la vitesse selon la formule: vitesse=distance parcourue/temps
 * Puis afficher le résultat dans le format suivant : 345 m/s(utiliser alert et console.log)
 */

let distanceTraveled = parseFloat(prompt("The distance traveled (m): "));
let timeSec = parseFloat(prompt("Time (sec) : "));
let speed = 0;

if (timeSec != 0) {
  speed = distanceTraveled / timeSec;

  console.log(`The speed is: ${speed} m/s`);
  alert(`The speed is: ${speed} m/s`);
} else {
  console.log(`The time cannot be 0`);
  alert(`The time cannot be 0`);
}

/**
 * 2. Ecrire un programme Javascript qui demande à l’utilisateur d’entrer à partir du clavier:
 * La longueur (m)
 * La largeur(m)
 * Puis calculer le périmètre selon la formule: perimetre=2(longueur+largeur)
 * Et la surface : surface=longueur*largeur
 */

let height = parseFloat(prompt("The height (m): "));
let width = parseFloat(prompt("The width (m): "));
let perimeter = 2 * (height + width);
let area = height * width;

console.log(`The perimeter is: ${perimeter} m`);
alert(`The perimeter is: ${perimeter} m`);
console.log(`The area is: ${area} m`);
alert(`The area is: ${area} m`);
