import { users } from "../data.js";
import { getParam } from "../utils/utils.js";

/*function getCurrentURL() {
  return window.location.href;
}

// Example
const url = new URL(getCurrentURL());
let params = new URLSearchParams(url.search);

let paramId = parseInt(params.get("id"));*/

let paramId = getParam("id");

let user = users.find((x) => x.id === paramId);

if (user !== null) {
  let namesTag = document.getElementById("names");
  namesTag.innerText = user.name;
  let mailTag = document.getElementById("mail");
  mailTag.innerText = user.email;
  let phoneTag = document.getElementById("phone");
  phoneTag.innerText = user.phone;
}
