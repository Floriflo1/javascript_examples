import { Row } from "../components/row.js";
import { users } from "../data.js";

// https://medium.com/swlh/urlsearchparams-in-javascript-df524f705317
function tableCreate() {
  const trAdd = document.querySelector("tr");

  trAdd.innerHTML += `<th>Action</th>`;

  let tableBody = document.querySelector("#users-list");

  users.forEach(
    ({
      id,
      name,
      email,
      phone,
      address: { city },
      company: { name: companyName },
    }) => {
      let u = {
        id,
        name,
        email,
        phone,
        city,
        companyName,
      };
      Row(tableBody, u);
      /* const tr = tablebody.insertRow();

      Object.values(u).forEach((e) => {
        let td = tr.insertCell();
        td.innerText = e;
      });

      let tdAction = tr.insertCell();
      tdAction.innerHTML = `<a href='pages/detail.html?id=${u.id}'>Voir</a>`;
      */
    }
  );

  /* Other method
  for (let user of users) {
    const tr = tablebody.insertRow();

    const td1 = tr.insertCell();
    td1.innerText = user.id;

    const td2 = tr.insertCell();
    td2.innerText = user.name;

    const td3 = tr.insertCell();
    td3.innerText = user.email;

    const td4 = tr.insertCell();
    td4.innerText = user.address.city;

    const td5 = tr.insertCell();
    td5.innerText = user.phone;

    const td6 = tr.insertCell();
    td6.innerText = user.company.name;

    const td7 = tr.insertCell();
    td7.innerHTML = `<a href='detail.html?id=${user.id}'>Voir</a>`;
  }*/

  /* Other method
  users.forEach((u) => {
    const tr = tablebody.insertRow();
    Object.keys(u).forEach((e) => {
      console.log(`key=${e}  value=${u[e]}`);
      if (
        e !== null &&
        (e === "id" ||
          e === "name" ||
          e === "email" ||
          e === "address" ||
          e === "phone" ||
          e === "company")
      ) {
        if (e === "address" || e === "company") {
          Object.keys(u[e]).forEach((ele) => {
            if (ele !== null && (ele === "city" || ele === "name")) {
              let td = tr.insertCell();
              td.innerText = u[e][ele];
            }
          });
        } else {
          let td = tr.insertCell();
          td.innerText = u[e];
        }
      }
    });
    let tdAction = tr.insertCell();
    tdAction.innerHTML = `<a href='detail.html?id=${u.id}'>Voir</a>`;
  });*/

  /* Other method
  for (let user of users) {

    userList.innerHTML += `<tr><td>${user.id}</td><td>${user.name}</td>

    <td>${user.email}</td><td>${user.address.city}</td><td>${user.phone}</td>

    <td>${user.company.name}</td><td><a href="http://127.0.0.1:5500/js-dom/Explication/indexTwo.html?id=${user.id}">Voir</a></td>`;

  }
 */
}

tableCreate();
