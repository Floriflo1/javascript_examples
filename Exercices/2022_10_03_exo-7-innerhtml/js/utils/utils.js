export function getCurrentURL() {
  return window.location.href;
}

export function getParam(param) {
  const url = new URL(getCurrentURL());
  let params = new URLSearchParams(url.search);

  return parseInt(params.get(param));
}
