export function Row(tableBody, user) {
  const tr = tableBody.insertRow();

  Object.values(user).forEach((e) => {
    let td = tr.insertCell();
    td.innerText = e;
  });

  let tdAction = tr.insertCell();
  tdAction.innerHTML = `<a href='pages/detail.html?id=${user.id}'>Voir</a>`;
}
