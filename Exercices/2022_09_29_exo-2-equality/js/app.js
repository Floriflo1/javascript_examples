/**
 * Écrire un programme Javascript qui demande l'âge d'un enfant. Ensuite, il l'informe de sa catégorie :
 * « Poussin » de 7 à 9 ans
 * « Pupille » de 10 à 11 ans
 * « Benjamin » de 12 à 13 ans
 * « Minime » de 14 à 15 ans
 * « Cadet » 16 à 17 ans
 */

let age = parseFloat(prompt("What is your age ? "));

if (typeof age !== "number") {
  if (age < 7) {
    console.log(`Your are to young to get a category`);
  } else {
    if (7 === age || age === 9) {
      console.log(`Your are a Poussin`);
    } else {
      if (10 <= age || age === 11) {
        console.log(`Your are a Pupille`);
      } else {
        if (12 <= age || age <= 13) {
          console.log(`Your are a Benjamin`);
        } else {
          if (14 <= age || age <= 15) {
            console.log(`Your are a Minime`);
          } else {
            if (16 <= age || age <= 17) {
              console.log(`Your are a Cadet`);
            } else {
              console.log(`Your are to old to get a category`);
            }
          }
        }
      }
    }
  }
}

/*if (7 <= age && age <= 9) {
  console.log(`Your are a Poussin`);
} else {
  if (10 <= age && age <= 11) {
    console.log(`Your are a Pupille`);
  } else {
    if (12 <= age && age <= 13) {
      console.log(`Your are a Benjamin`);
    } else {
      if (14 <= age && age <= 15) {
        console.log(`Your are a Minime`);
      } else {
        if (16 <= age && age <= 17) {
          console.log(`Your are a Cadet`);
        }
      }
    }
  }
} */
