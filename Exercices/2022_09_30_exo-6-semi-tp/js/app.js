// E1
const users1 = [
  { noms: "Jojo Jack", age: 21 },
  { noms: "Paul-Jules", age: 45 },
  { noms: "Pierre Zozor", age: 67 },
  { noms: "Gilbert Toto", age: 39 },
  { noms: "Simon Pierre", age: 35 },
  { noms: "Andres Iniesta", age: 70 },
];
/**
 * 1 - Avoir un tableau des users dont l'age varie entre 20-40
 * 2 - Recuprerer le user dont noms est Pierre Zozor
 * 3 - Recupere un tableau ne contenant que les noms des utilisateurs
 */

// 1 - Avoir un tableau des users dont l'age varie entre 20-40
const usersBetween20And40 = users1.filter((user) => {
  return 20 <= user.age && user.age <= 40;
});
console.log("Users between 20 and 40 : ");
console.log(usersBetween20And40);

// 2 - Recuprerer le user dont noms est Pierre Zozor
const namePierreZozor = users1.filter((user) => {
  return user.noms === "Pierre Zozor";
});
console.log("User with name Pierre Zozor : ");
console.log(namePierreZozor);

// E2
const users = [
  {
    id: 1,
    sexe: "M",
    nom: "Lisangola",
    prenom: "Bondjali",
    nationalite: "Allemande",
  },
  {
    id: 2,
    sexe: "M",
    nom: "Alain",
    prenom: "Stanislas",
    nationalite: "Bresilienne",
  },
  { id: 3, sexe: "M", nom: "Kavov", prenom: "Joseph", nationalite: "Russe" },
  {
    id: 4,
    sexe: "M",
    nom: "Jean",
    prenom: "Jonathan",
    nationalite: "Francaise",
  },
  {
    id: 5,
    sexe: "F",
    nom: "Lisangola",
    prenom: "Alain",
    nationalite: "Italienne",
  },
  {
    id: 6,
    sexe: "F",
    nom: "Sakura",
    prenom: "Josephine",
    nationalite: "Bresilienne",
  },
  {
    id: 7,
    sexe: "M",
    nom: "Le Blanc",
    prenom: "Axel",
    nationalite: "Italienne",
  },
  {
    id: 8,
    sexe: "M",
    nom: "Alison",
    prenom: "Murdoch",
    nationalite: "Somalienne",
  },
  { id: 9, sexe: "F", nom: "Zarosky", prenom: "Semia", nationalite: "Russe" },
  {
    id: 10,
    sexe: "F",
    nom: "Ali",
    prenom: "Laurene",
    nationalite: "Algerienne",
  },
];

// 1 - Trouver le nombre des Algeriens dans notre collection
const algeriens = users.filter((user) => {
  return user.nationalite.toLowerCase().includes("algerien");
});
console.log(`1 - Nb Algeriens : ${algeriens.length}`);

//2 - Trouver toutes les personnes dont les prenoms se terminent par a
const allPeopleEndWithA = users.filter((user) => {
  //return user.prenoms[-1] === "a";
  return user.prenom.toLowerCase().endsWith("a");
});
console.log("2 - All people end with a : ");
console.log(allPeopleEndWithA);

//3 - Avoir la liste tous toutes les noms
const userAllNames = users.map((user) => {
  const { nom: nom } = user;
  return nom;
});
console.log("3 - All people name : ");
console.log(userAllNames);

//4 - La liste de tous les hommes russes
const russianUsers = users.filter((user) => {
  return user.nationalite.toLowerCase() === "russe";
});
console.log("4 - All russian users : ");
console.log(russianUsers);

//5 - La liste de toutes les personnes dont les noms commencent par K et qui sont des femmes
const userBeginWithKandWoman = users.filter((user) => {
  //return user.sexe.toLowerCase() === "f" && user.nom[0].toLowerCase() === "k";
  return (
    user.sexe.toLowerCase() === "f" && user.nom.toLowerCase().startsWith("k")
  );
});
console.log(`5 - User begin with K and woman : `);
console.log(userBeginWithKandWoman);

//6 - La liste de tous les hommes dont les noms commencent par a
const userBeginWithMandMan = users.filter((user) => {
  //return user.sexe.toLowerCase() === "m" && user.nom[0].toLowerCase() === "a";
  return (
    user.sexe.toLowerCase() === "m" && user.nom.toLowerCase().startsWith("a")
  );
});
console.log("6 - User begin with A and man : ");
console.log(userBeginWithMandMan);
// Utiliser uniquement les nouvelles méthodes de ES6
