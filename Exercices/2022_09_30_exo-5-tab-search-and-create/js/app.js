// Création d'un tableau vide
/**
 * E1.A partir du tableau suivant : const fruits = ['pomme', 'mangue', 'banane', 'orange'], créez un nouveau tableau avec tous les mots en majuscule.
 * E2.A partir du tableau const fruits = ['pomme', 'mangue', 'banane', 'ananas', 'orange', 'citron']; Créer un tableau avec tous les fruits dont les noms contiennent la lettre m
 */

// E1
const fruits = ["pomme", "mangue", "banane", "orange"];

const fruitsUpper = fruits.map((element) => {
  return element.toUpperCase();
});

console.log(fruitsUpper);

// E2
const fruits2 = ["pomme", "mangue", "banane", "ananas", "orange", "citron"];

const fruitsA = fruits2.filter((str) => {
  return str.includes("m");
});

console.log(fruitsA);
